'use strict';

function randomElt(a) {
    var len = a.length;
    var index = len > 1 ? Math.floor(Math.random() * len) : 0;
    return a[index];
}

var slides = [
    { text: "Balades", imgs: ["bicicletta"], url: '/balades' },
    { text: "Convergence", imgs: ["convergence", "convergence2"], url: '/convergence-francilienne/' },
    { text: "Atelier", imgs: [ "tableau_outils_perpignan" ], url: '/ateliers/' },
];
randomElt(slides).active = true;

var indicatorsElt = $('ol.carousel-indicators');
var slidesElt = $('.carousel-inner');
$.each(slides, function (index, slide) {
    var img = randomElt(slide.imgs);
    indicatorsElt.append('<li data-target="#carousel-diaporama-generic" data-slide-to="' + index + '" class="' + (slide.active ? 'active' : '') + '"></li> ');
    slidesElt.append(
        '<div class="item mdb-home-carousel-img ' + (slide.active ? 'active' : '') + '" style="background-image: url(' + template_url + '/img/' + img + '.jpg)">' +
            '<div class="carousel-caption">' +
            '<h5 class="carousel-description"><a href="' + slide.url + '">' + slide.text + '</a></h5>' +
            '</div>' +
            '</div>');
});

$('.carousel').carousel({
    interval: 10000,
    pause: "hover",
    wrap: true,
    keyboard: true
});

$(".voirplus").on('click', function() {
    $(this).parent().addClass('voirEnEntier');
});

$("#btnrecherche button").on('click', function () {
    var input = $("#btnrecherche input");
    if (input.val() === '') {
        input.focus();
        return false;
    } else {
        return true;
    }    
});
