<?php

if ($_SERVER['REQUEST_URI'] === '/refresh-calendar-feeds') {
    //$calendar_id;
    function mdb_filter_google_feed($calendar) {
        //global $calendar_id;
        //echo "mdb_filter_google_feed " . $calendar_id . ' ' . $calendar['title'] . "\n<br>";
        foreach ($calendar['events'] as &$day_events) {
            foreach ($day_events as &$event) {
                if (preg_match("!(?:^|\n)\n?(https?://\\S*)$!", $event['description'], $m, PREG_OFFSET_CAPTURE)) {
                    $event['description'] = substr($event['description'], 0, $m[0][1]);
                    $event['link'] = $m[1][0];
                    //echo("got link " . $event['link'] . "\n");
                //} else if ($calendar_id === 17290) {
                //    // compact
                //    $event['link'] = "/agenda#" . $event["uid"];
                } else if (preg_match('/Marquage/i', $event['title'])) {
                    $event['link'] = '/marquage';
                } else if (preg_match('/MDV/i', $event['title'])) {
                    $event['link'] = '/category/maison-du-velo/';
                } else {
                    $event['link'] = '/agenda#' . $event["uid"];
                }
            }
        }
        return $calendar;
    }
    $mdb_calendars = get_posts(['post_type' => 'calendar']);
    foreach ($mdb_calendars as $calendar) {
        $calendar_id = $calendar->ID;
        echo "deleting cache for $calendar_id\n<br>";
        $transient = '_simple-calendar_feed_id_' . $calendar_id . '_google';
        delete_transient($transient);
        add_filter("pre_set_transient_$transient", 'mdb_filter_google_feed');
    }
    foreach ($mdb_calendars as $calendar) {
        $calendar_id = $calendar->ID;
        echo "getting feed $calendar_id...";
        do_shortcode('[calendar id="' . $calendar_id . '" title=""]');
        //echo " done\n<br>";
    }
    header("HTTP/1.1 200 OK");
    exit(0);
}

get_header();

?>

    <section class="fondu-anim container-fluid">
        <div class="row">
            <div class="col-xs-12 text-center titre2">
                <h2 class="fondu-anim2"><?php bloginfo('name'); ?></h2>
            </div>
        </div>
    </section>

    <section class="center container">
      <div class="fondu-anim2" style="padding-bottom: 6em">
        <h1 class="titre3">Page non trouvée</h1>

        La page demandée n'a pas pu être trouvée.
      </div>
    </section>

<?php
    
	get_template_part('content', 'mosaique' );
get_footer();
?>
   
