<?php

add_theme_support('post-thumbnails' );


add_action( 'after_setup_theme', 'register_my_menu' );
function register_my_menu() {
  register_nav_menu( 'primary', 'Menu principal' );
  register_nav_menu( 'antenne', 'Menu des relais locaux' );
}

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Pied de page 1', 'mdb' ),
        'id' => 'footer-sidebar-1',
        'description' => __( 'Widgets zone 1 de la sidbar', 'mdb' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
	
	 register_sidebar( array(
        'name' => __( 'Pied de page 2', 'mdb' ),
        'id' => 'footer-sidebar-2',
        'description' => __( 'Widgets zone 2 de la sidbar.', 'mdb' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
	
	 register_sidebar( array(
        'name' => __( 'Pied de page 3', 'mdb' ),
        'id' => 'footer-sidebar-3',
        'description' => __( 'Widgets zone 3 de la sidbar', 'mdb' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget'  => '</li>',
	'before_title'  => '<h6 class="widgettitle">',
	'after_title'   => '</h6>',
    ) );
}

add_theme_support( 'automatic-feed-links' );


// disallow h1 (used at toplevel, too big), h5&h6 (too small)
add_filter( 'tiny_mce_before_init', function ($init_array) {  
    $init_array['block_formats'] =
        'Paragraph=p;' .
        'Heading 2=h2;' .
        'Heading 3=h3;' .
        'Heading 4=h4;' .
        'Preformatted=pre';
    return $init_array;      
});  


// accept [link href=/foo]xxx[/link] in widget titles (notice the missing quotes for href)
add_filter( 'widget_title', function($mytitle) { 
    $mytitle = str_replace( '[link', '<a', $mytitle );
    $mytitle = str_replace( '[/link]', '</a>', $mytitle );
    $mytitle = str_replace( ']', '>', $mytitle );
    return $mytitle;
});

function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
// Remove image size attributes from post thumbnails + from images added to a WordPress post
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );

/*
add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = 'to';
 
    if ( isset( $atts[$my_attr] ) ) {
        $out[$my_attr] = $atts[$my_attr];
    }
 
    return $out;
}
*/

// if you configure a contact form with Additional Header "List-Post: [_url]",
// it will take the last part of the url as the email address
// (especially used in [contact_antenne] form)
add_filter('wpcf7_mail_components', function($components) {
    if (preg_match('!List-Post: .*/([\w.-]+)/?$!', $components['additional_headers'], $m)) {        
        $components['recipient'] = $m[1] . '@mdb-idf.org';
    }
    return $components;
}); 

function contact_button_and_form($contact_id ){
    $button_text = 'Nous envoyer un mél';
    $contact = do_shortcode('[contact-form-7 id="' . $contact_id . '" title=""]');
    $button = "<button class='mdb-contact-button btn btn-primary' type='button' data-toggle='collapse' data-target='#collapseContactAntenne' aria-expanded='false' aria-controls='collapseContactAntenne'>$button_text</button>";
    return "$button<div class='collapse' id='collapseContactAntenne'><div class='well'>$contact</div></div>";
}
function contact_mdb($atts) {
    return contact_button_and_form(17417);
}
add_shortcode('contact_antenne', function($atts) {
    return contact_button_and_form(17410);
});
add_shortcode('contact_mdb', function($atts) {
    return contact_button_and_form(17417);
});
add_shortcode('contact_balades', function($atts) {
    return contact_button_and_form(17655);
});

// allow sticky posts in widget "Recent articles"
add_filter('widget_posts_args', function($args) {
    $args['post__in'] = get_option( 'sticky_posts' );
    return $args;
});



?>


