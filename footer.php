 <!--début du footer-->

    <footer class="fondu-anim footer-text container-footer row">
        <div class="col-md-4 col-sm-6">
            <?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) : ?>
		<ul class="mdb-sidebar">
		    <?php dynamic_sidebar( 'footer-sidebar-1' ); ?>
		</ul>
	    <?php endif; ?>
        </div>

        <div class="col-md-4 col-sm-6">
            <?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) : ?>
		<ul class="mdb-sidebar">
		    <?php dynamic_sidebar( 'footer-sidebar-2' ); ?>
		</ul>
	    <?php endif; ?>
        </div>
            
        <div class="col-md-4 col-sm-12">
            <?php if ( is_active_sidebar( 'footer-sidebar-3' ) ) : ?>
		<ul class="mdb-sidebar"> 
		    <?php dynamic_sidebar( 'footer-sidebar-3' ); ?>
		</ul>
	    <?php endif; ?>
        </div>
    </footer>

    <!--fin du footer-->

    <section class="fondu-anim container-social">

        <p class="text-center mentions-legales"><a href="<?= esc_url( home_url('/statuts/') ); ?>">Mentions légales</a> | <a href="<?= esc_url( home_url('/wp-admin/') ); ?>">Espace contributeurs</a></p>
        
    </section>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script>
     var template_url = '<?php bloginfo('template_url'); ?>';
    </script>
    <script src="<?php bloginfo('template_url'); ?>/js/theme.js"></script>
    
    <?php wp_footer(); ?>
</body>

</html>