<?php
$mosaique = [
    [ 'id' => 'agenda', 'title' => 'Agenda', 'text' => 'des événements', 'color' => 3, 'url' => '/agenda/' ],
    [ 'id' => 'bicycode', 'title' => 'Marquage', 'text' => 'de votre vélo', 'color' => 2, 'url' => '/marquage/' ],
    [ 'id' => 'code', 'color' => 1, 'title' => 'Code', 'text' => 'de la rue', 'url' => '/category/dossiers/code-de-la-rue/' ],
    [ 'id' => 'carte', 'color' => 1, 'title' => 'Antennes', 'text' => 'de MDB', 'url' => '/relais-locaux/' ],
    [ 'id' => 'atelier', 'color' => 3, 'title' => 'Atelier', 'text' => 'vélo', 'url' => '/ateliers/' ],
    [ 'id' => 'velo-ecole', 'color' => 2, 'title' => 'Ecole', 'text' => 'de vélo', 'url' => '/category/velo-ecoles/' ],
    [ 'id' => 'balades', 'color' => 2, 'title' => 'Balades', 'text' => 'en île-de-France', 'url' => '/balades/' ],
    [ 'id' => 'bourses-aux-velos', 'color' => 1, 'title' => 'Bourses', 'text' => 'aux vélos', 'url' => '/bourses-aux-velos/' ],
    [ 'id' => 'convergence', 'color' => 3, 'title' => 'Convergence', 'text' => 'événement annuel', 'url' => '/convergence-francilienne/' ],   
];
?>

<section class="center container mosaique">
    <div class="row">
        <?php
        $time = 0.25;
        foreach ($mosaique as $elt) {
            $time += 0.3;
            $img = get_template_directory_uri() . "/img/mosaique_" . $elt['id'] . ".jpg";
            $style = "animation-delay: 0s, ${time}s;";
            $class = "fondu-anim-mosaique sous-titre-mosaique col-sm-4 col-xs-6 mosaique-box mosaique-color-" . $elt['color'];
        ?>
            <a href="<?= esc_url(home_url($elt['url'])) ?>">
                <div style="<?= $style ?>" class="<?= $class ?>">
                    <img src="<?= $img ?>">
                    <h5><?= $elt['title'] ?></h5>
                    <p><?= $elt['text'] ?></p>
                </div>
            </a>
       <?php } ?>       
    </div>
</section>
