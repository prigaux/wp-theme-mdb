<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MDB - <?php is_category() ? single_cat_title() : the_title() ?></title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="<?= get_template_directory_uri() ?>/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
     <?php wp_head(); ?>
</head>

<body class="<?= is_front_page() ? "homePage" : "" ?>">

    <!--début de la nav-->

    <nav class="fondu-anim-menu change-color-menu mynavbar navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- LOGO -->

                <a class="fondu-anim-menu2 logo" href="<?= esc_url( home_url( '/' ) ); ?>">
                    <img src="<?= get_template_directory_uri() ?>/img/logomdb-.svg">
                </a>

                <!-- LOGO -->

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <?php 
    
            require_once('wp-bootstrap-navwalker.php');

	    $arg = array( 'menu' => 'primary', 'container' => 'div',
                          'menu_class' => 'nav navbar-nav', 'echo' => true,
                          'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                          'item_spacing' => 'preserve',
                          'depth' => 0,
                          'theme_location' => 'primary',
                          'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback', 'walker'  => new WP_Bootstrap_Navwalker());
			
			
			
	    wp_nav_menu($arg); ?>

                <!--RESEAUX SOCIAUX-->

                <div class="fondu-anim-menu3 social-container hidden-xs">
                    
                    <a href="https://twitter.com/mdbidf" title="Twitter"><img src="<?= get_template_directory_uri() ?>/img/logo_white_twitter_solo.png"></a>
                    <a href="https://framasphere.org/u/mdb" title="Diaspora*"><img src="<?= get_template_directory_uri() ?>/img/logo_white_diaspora_solo.svg"></a>
                    <a href="https://www.facebook.com/MieuxseDeplaceraBicyclette/" title="Facebook"><img src="<?= get_template_directory_uri() ?>/img/logo_white_fb_solo.png"></a>
                
                </div>

                <!--RESEAUX SOCIAUX-->

                <!--SEARCH ENGINE-->

                <form id="btnrecherche" class="fondu-anim-menu2 navbar-form navbar-right" action="<?= esc_url( home_url( '/' ) ); ?>" role="search">
                    <div class="form-group">
                        <input type="text" name="s" class="form-control" placeholder="Rechercher">
                    </div>
                    <button type="submit"  class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </form>

                <!--SEARCH ENGINE-->
            </div>

                
            </div>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!--fin de la nav-->
    <!--début du slider-->

    <section class="fondu-anim container-fluid mdb-home-carousel">
        <div id="carousel-diaporama-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators"></ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
            </div>

            <!-- Slider Controls -->
            <a class="left carousel-control" href="#carousel-diaporama-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-diaporama-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <!-- Slider Controls -->

             <!--h1 class="nomdusite"><span class="mdb-one">MIEUX SE DÉPLACER A</span><br><span class="mdb-two">BICYCLETTE</span></h1-->
        </div>
    </section>

    <!--fin du slider-->
    <!--début de l'en-tête-->
