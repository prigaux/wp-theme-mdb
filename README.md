Formulaires de contact
----------------------

- pour mettre un formulaire de contact qui envoie à la permanence, mettre `[contact_mdb]` ([exemple d'utilisation](http://mdb-idf.org/contact/))

- pour mettre un formulaire de contact antennes : sur une page http://mdb-idf.org/xxx/zzz/, mettre `[contact_antenne]`, ça envoie un mail à zzz@mdb-idf.org ([exemple d'utilisation](http://mdb-idf.org/category/nos-relais-locaux/paris/mdb18/))

- et sinon on peut créer un lien vers http://mdb-idf.org/contacter/?/zzz : c'est un formulaire de contact vers zzz@mdb-idf.org

Agendas
-------

L'agenda principal de MDB provient des événements du google agenda [Actions de MDB](https://calendar.google.com/calendar/embed?src=l1cqsm397mhq3fu0qlerp0tpg0%40group.calendar.google.com). Si vous voulez ajouter des événements, transmettez les à la permanence MDB, ou demandez à pourvoir ajouter directement dans le google agenda.

* liens : vous pouvez mettre un lien vers une page web (notamment un article dans wordpress), en terminant la description de l'événement par le lien.
* mise à jour : les agendas google sont mis à jour toutes les heures via la « tâche planifiée » qui appelle http://mdb-idf.org/refresh-calendar-feeds . Vous pouvez forcer cette mise à jour. Ce script extrait les liens de la description des événements
* marquage et bourses aux vélos : les événements de l'agenda "Actions de MDB" sont visibles dans les calendriers wordpress [Agenda marquage](http://mdb-idf.org/category/marquage/) et [Agenda des bourses aux vélos](http://mdb-idf.org/category/bourses-aux-velos/).
 
Antennes
--------

* chaque antenne a un lien court, par exemple http://mdb-idf.org/paris18, le nom court zzz doit correspondre à l'adresse mail zzz@mdb-idf.org (cf Formulaires de contact)
* le lien court renvoie vers une catégorie, par exemple http://mdb-idf.org/category/nos-relais-locaux/paris/mdb18/ , qui affiche tous les articles de la catégorie "Paris 18".

**Envoyer des e-mails avec une adresse ou un alias différents**

- thunderbird : https://support.mozilla.org/fr/kb/utiliser-identites-thunderbird
- gmail : https://support.google.com/mail/answer/22370?hl=fr



Création de compte
------------------

J'ai créé ton compte.

Ton login est ton adresse mél

Il faut que tu choisisses un mot de passe. Pour cela, il suffit :
* d'aller sur « Espace contributeurs » en pied de page (https://mdb-idf.org/wp-admin/)
* et de cliquer sur « Mot de passe oublié ? » (https://mdb-idf.org/wp-login.php?action=lostpassword)
