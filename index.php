<?php

get_header();

$query = get_queried_object();

$compact = is_category() || is_search() || is_author();

function mdb_category_info($category) {
    $link = esc_url( get_category_link($category->term_id) );
    $name = esc_html($category->name);
    return "<span class='has-glyphicon glyphicon-folder-open'><a href='$link'>$name</a></span>";
}

?>

    <section class="fondu-anim container-fluid">
        <div class="row">
            <div class="col-xs-12 text-center titre2">
                <h2 class="fondu-anim2"><?php bloginfo('name'); ?></h2>
            </div>
        </div>
    </section>

    <!--fin de l'en-tête-->

    <section class="center container">
        <div class="fondu-anim2">

	    <?php if (is_category()) { ?>
                <h1 class="titre3"><?php single_cat_title() ?></h1>
                <?php
                if ($query->parent) {
                    echo '<div class="mdb-content-header" style="margin-bottom: 2em">';
                    echo mdb_category_info(get_category($query->parent));
                    echo '</div>';
                }
	        $category_description = category_description();
	        if ($category_description) {
	            echo "<div style='margin-bottom: 3em'>$category_description</div>";
	        }
             }
             if (is_author()) {
                    the_post();
                    $author = get_the_author();
                    rewind_posts(); ?>
                    <h1 class="titre3"><?= $author ?></h1>
                    <?php
             }
           
             while ( have_posts() ) : the_post();
                if ($compact) { ?>
                    <h4><a class="titre4" href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                <?php } else { ?>
    		    <h1 class="titre3"><?php the_title(); ?></h1>
                <?php } ?>
           
                <div class="mdb-content-header">
                  <?php if (!is_page() && get_post_type() !== 'page') { ?>
                    <time class="glyphicon-time has-glyphicon" datetime="<?= esc_attr(get_the_date('c')) ?>"><?= esc_html( get_the_date() ) ?></time>
                   <?php
                     foreach (get_the_category() as $category) {
                       if (is_category() && $category->term_id === $query->term_id) continue; // skip current category
                       echo mdb_category_info($category);
	             }
                   }
                   ?>
                </div>
                <div class="mdb-content">
		    <?php
                    if ($compact) {
		        the_excerpt();
                    } else {
                        the_content( 'Lire la suite' );
                    }
                    ?>
                </div>
                <?php if (!$compact && !is_front_page()) { ?>
                    <ul class="share-buttons">
                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://mdb-idf.org&t=" title="Partager sur Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"><img alt="Partager sur Facebook" src="<?= get_template_directory_uri() ?>/img/flat_web_icon_set/black/Facebook.png"></a></li>
                        <li><a href="javascript:;" onclick="window.open('https://share.diasporafoundation.org/?url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title),'das','location=no,links=no,scrollbars=no,toolbar=no,width=620,height=550'); return false;" rel="nofollow" target="_blank" title="Partager sur Diaspora*"><img style="height: 32px; width: 32px" alt="Partager sur Diaspora*" src="<?= get_template_directory_uri() ?>/img/diaspora-dans-rond-noir.svg"></a></li>
                        <li><a href="https://twitter.com/intent/tweet?source=http://mdb-idf.org&text=:%20http://mdb-idf.org&via=MDBIDF" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + '%20:%20'  + encodeURIComponent(document.URL)); return false;"><img alt="Tweet" src="<?= get_template_directory_uri() ?>/img/flat_web_icon_set/black/Twitter.png"></a></li>
                    </ul>
                <?php } ?>                
	   <?php endwhile; // end of the loop. ?>

           <?php
           if (get_next_posts_link()) {
               next_posts_link(">>> Plus d'articles", 0 );
           } ?>
        </div>
        
    </section>
    <br>

   
   <?php 
 if (is_front_page()) {
	get_template_part('content', 'mosaique' );
}
?>
    
    
    <?php get_footer(); ?>
   
